# Li1S Charge Protect Boost with an enable pin
Charge-Protect-Boost board for 1s Lithium battery, boost Output 5V

Pull EN High to activate Boost Converter.
EN is pulled to GND with a 10k resistor, so the default state of the Boost Converter is off.
The battery voltage is still applied to the 5V pin even if the boost converter is off.

Charging current set to 130ma (R=10kOm)

![Image of PCB](/PCB.png)
