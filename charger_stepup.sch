EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED D2
U 1 1 5980CCB4
P 1700 2550
F 0 "D2" H 1700 2650 50  0000 C CNN
F 1 "LED" H 1700 2450 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 1700 2550 50  0001 C CNN
F 3 "" H 1700 2550 50  0001 C CNN
	1    1700 2550
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D1
U 1 1 5980CE60
P 1700 2250
F 0 "D1" H 1700 2350 50  0000 C CNN
F 1 "LED" H 1700 2150 50  0000 C CNN
F 2 "LED_SMD:LED_0603_1608Metric" H 1700 2250 50  0001 C CNN
F 3 "" H 1700 2250 50  0001 C CNN
	1    1700 2250
	-1   0    0    1   
$EndComp
Text GLabel 1100 2100 0    39   Input ~ 0
IN+
Text GLabel 1100 3150 0    39   Input ~ 0
GND
$Comp
L Device:C C2
U 1 1 598D1488
P 6100 2550
F 0 "C2" H 6125 2650 50  0000 L CNN
F 1 "0.1uF" H 6125 2450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6138 2400 50  0001 C CNN
F 3 "" H 6100 2550 50  0001 C CNN
	1    6100 2550
	1    0    0    -1  
$EndComp
$Comp
L dw01:DW01 U2
U 1 1 598D23F7
P 5250 2400
F 0 "U2" H 5350 2750 60  0000 C CNN
F 1 "FS312F-G" H 5150 2750 60  0000 R CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-6_Handsoldering" H 5200 2100 60  0001 C CNN
F 3 "" H 5200 2100 60  0001 C CNN
	1    5250 2400
	1    0    0    -1  
$EndComp
$Comp
L tp4056:TP4056 U1
U 1 1 597F50E3
P 2900 2350
F 0 "U1" H 2550 2750 60  0000 L CNN
F 1 "TP4056" H 3250 2750 60  0000 R CNN
F 2 "lib:TP4056_SOP-8-PP" H 2900 2350 60  0001 C CNN
F 3 "" H 2900 2350 60  0000 C CNN
	1    2900 2350
	1    0    0    -1  
$EndComp
NoConn ~ 4700 2250
Text GLabel 7050 3000 2    39   Input ~ 0
BAT-
Text Notes 3250 2800 0    39   ~ 0
Set Charging\n Current to 130ma\n
Wire Wire Line
	3400 2500 3450 2500
Wire Wire Line
	3400 2100 4150 2100
Wire Wire Line
	2900 3150 2900 2750
Wire Wire Line
	3400 2400 3850 2400
Wire Wire Line
	3850 2400 3850 2500
Wire Wire Line
	3850 2500 3750 2500
Connection ~ 3850 2500
Wire Wire Line
	1550 2250 1450 2250
Wire Wire Line
	1450 2100 1450 2250
Wire Wire Line
	1450 2550 1550 2550
Connection ~ 1450 2250
Wire Wire Line
	1300 2500 1300 2100
Connection ~ 1300 2100
Wire Wire Line
	1300 2800 1300 3150
Wire Wire Line
	5850 2250 6100 2250
Connection ~ 6100 2250
Wire Wire Line
	6100 2400 6100 2250
Wire Wire Line
	5850 2450 5900 2450
Wire Wire Line
	5900 2450 5900 3000
Wire Wire Line
	4700 2450 4600 2450
Wire Wire Line
	4600 2450 4600 2550
Wire Wire Line
	6750 2250 6900 2250
Wire Wire Line
	6900 1950 6900 2250
Connection ~ 6900 2250
$Comp
L Device:C C3
U 1 1 5996DEBE
P 4150 2700
F 0 "C3" H 4175 2800 50  0000 L CNN
F 1 "10uF" H 4175 2600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4188 2550 50  0001 C CNN
F 3 "" H 4150 2700 50  0001 C CNN
	1    4150 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 2550 4150 2100
Wire Wire Line
	4150 3150 4150 2850
Wire Wire Line
	5400 2900 5400 2850
Connection ~ 3850 3150
Wire Wire Line
	6100 2700 6100 3000
Connection ~ 6100 3000
Connection ~ 6900 3000
Wire Wire Line
	2900 3150 3850 3150
Wire Wire Line
	3850 2500 3850 3150
Wire Wire Line
	1450 2250 1450 2550
Wire Wire Line
	1300 2100 1450 2100
Wire Wire Line
	6100 2250 6450 2250
Wire Wire Line
	3850 3150 4150 3150
Wire Wire Line
	5900 3000 6100 3000
Wire Wire Line
	6100 3000 6900 3000
Wire Wire Line
	6900 3000 7050 3000
Wire Wire Line
	1900 2250 1850 2250
Wire Wire Line
	1900 2550 1850 2550
Wire Wire Line
	1450 2100 2350 2100
Connection ~ 1450 2100
Wire Wire Line
	2350 2100 2350 2200
Wire Wire Line
	2400 2100 2350 2100
Connection ~ 2350 2100
Wire Wire Line
	2400 2200 2350 2200
Connection ~ 2350 2200
Wire Wire Line
	2350 2200 2350 2300
Wire Wire Line
	2400 2300 2350 2300
Wire Wire Line
	2200 2250 2250 2250
Wire Wire Line
	2250 2250 2250 2400
Wire Wire Line
	2250 2400 2400 2400
Wire Wire Line
	2200 2550 2250 2550
Wire Wire Line
	2250 2550 2250 2500
Wire Wire Line
	2250 2500 2400 2500
Wire Wire Line
	2900 3150 1300 3150
Connection ~ 2900 3150
Connection ~ 1300 3150
$Comp
L power:GND #PWR01
U 1 1 5ED1FDD4
P 2900 3300
F 0 "#PWR01" H 2900 3050 50  0001 C CNN
F 1 "GND" H 2905 3127 50  0000 C CNN
F 2 "" H 2900 3300 50  0001 C CNN
F 3 "" H 2900 3300 50  0001 C CNN
	1    2900 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 3300 2900 3150
Wire Wire Line
	1100 2100 1150 2100
Wire Wire Line
	1100 3150 1150 3150
Wire Wire Line
	1000 2700 1150 2700
Wire Wire Line
	1150 2700 1150 3150
Connection ~ 1150 3150
Wire Wire Line
	1150 3150 1300 3150
Wire Wire Line
	1150 2100 1150 2600
Wire Wire Line
	1150 2600 1000 2600
Connection ~ 1150 2100
Wire Wire Line
	1150 2100 1300 2100
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 5ED2E00D
P 800 2700
F 0 "J1" H 718 2375 50  0000 C CNN
F 1 "5V_Charge" H 718 2466 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 800 2700 50  0001 C CNN
F 3 "~" H 800 2700 50  0001 C CNN
	1    800  2700
	-1   0    0    1   
$EndComp
NoConn ~ 4750 3450
NoConn ~ 5750 3450
$Comp
L fs8205a:FS8205A U3
U 1 1 59976E41
P 5350 3250
F 0 "U3" H 5400 3250 60  0000 C CNN
F 1 "FS8205A" H 5400 3100 60  0000 C CNN
F 2 "Housings_SSOP:TSSOP-8_4.4x3mm_Pitch0.65mm" H 6300 3500 60  0001 C CNN
F 3 "" H 6300 3500 60  0001 C CNN
	1    5350 3250
	0    1    1    0   
$EndComp
Wire Wire Line
	5400 2900 5800 2900
Wire Wire Line
	5800 2900 5800 3050
Wire Wire Line
	5800 3050 5750 3050
Wire Wire Line
	4750 3050 4700 3050
Wire Wire Line
	4750 3350 4700 3350
Wire Wire Line
	4700 3350 4700 3150
Wire Wire Line
	4700 3150 4750 3150
Wire Wire Line
	4700 3150 4600 3150
Wire Wire Line
	4600 3150 4600 2850
Connection ~ 4700 3150
Wire Wire Line
	5750 3150 5800 3150
Wire Wire Line
	5800 3150 5800 3350
Wire Wire Line
	5800 3350 5750 3350
Wire Wire Line
	5800 3150 6100 3150
Wire Wire Line
	6100 3150 6100 3000
Connection ~ 5800 3150
Wire Wire Line
	4700 3050 4700 2900
Wire Wire Line
	4700 2900 5100 2900
Wire Wire Line
	5100 2900 5100 2850
$Comp
L Connector_Generic:Conn_01x02 J3
U 1 1 5ED7F80F
P 7100 2550
F 0 "J3" H 7180 2542 50  0000 L CNN
F 1 "Battery" H 7180 2451 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7100 2550 50  0001 C CNN
F 3 "~" H 7100 2550 50  0001 C CNN
	1    7100 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 2250 6900 2550
Wire Wire Line
	6900 2650 6900 3000
$Comp
L Regulator_Switching:MT3608 U4
U 1 1 5ED87F5B
P 8600 2350
F 0 "U4" H 8600 2717 50  0000 C CNN
F 1 "MT3608" H 8600 2626 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 8650 2100 50  0001 L CIN
F 3 "https://www.olimex.com/Products/Breadboarding/BB-PWR-3608/resources/MT3608.pdf" H 8350 2800 50  0001 C CNN
	1    8600 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:L L1
U 1 1 5EDA4CED
P 8600 1900
F 0 "L1" V 8790 1900 50  0000 C CNN
F 1 "22uH" V 8699 1900 50  0000 C CNN
F 2 "Inductor_SMD:L_7.3x7.3_H4.5" H 8600 1900 50  0001 C CNN
F 3 "~" H 8600 1900 50  0001 C CNN
	1    8600 1900
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R8
U 1 1 5EDA62C7
P 9200 2650
F 0 "R8" V 9280 2650 50  0000 C CNN
F 1 "1k" V 9200 2650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9130 2650 50  0001 C CNN
F 3 "" H 9200 2650 50  0001 C CNN
	1    9200 2650
	-1   0    0    1   
$EndComp
$Comp
L Device:R R7
U 1 1 5EDA6A89
P 9200 2250
F 0 "R7" V 9280 2250 50  0000 C CNN
F 1 "7k5" V 9200 2250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 9130 2250 50  0001 C CNN
F 3 "" H 9200 2250 50  0001 C CNN
	1    9200 2250
	-1   0    0    1   
$EndComp
$Comp
L Device:D_Schottky D3
U 1 1 5EDA7161
P 9000 1900
F 0 "D3" H 9000 1683 50  0000 C CNN
F 1 "SS34" H 9000 1774 50  0000 C CNN
F 2 "Diode_SMD:D_SMA" H 9000 1900 50  0001 C CNN
F 3 "~" H 9000 1900 50  0001 C CNN
	1    9000 1900
	-1   0    0    1   
$EndComp
Wire Wire Line
	8750 1900 8800 1900
Wire Wire Line
	8800 1900 8800 2050
Wire Wire Line
	8800 2050 8950 2050
Wire Wire Line
	8950 2050 8950 2250
Wire Wire Line
	8950 2250 8900 2250
Connection ~ 8800 1900
Wire Wire Line
	8800 1900 8850 1900
Wire Wire Line
	8900 2450 9200 2450
Wire Wire Line
	9200 2450 9200 2400
Wire Wire Line
	9200 2450 9200 2500
Connection ~ 9200 2450
Wire Wire Line
	9200 2100 9200 1900
Wire Wire Line
	9200 1900 9150 1900
Wire Wire Line
	9200 2800 9200 2850
Wire Wire Line
	9200 2850 8600 2850
Wire Wire Line
	8600 2850 8600 2650
$Comp
L power:GND #PWR02
U 1 1 5EDBDE33
P 8600 2900
F 0 "#PWR02" H 8600 2650 50  0001 C CNN
F 1 "GND" H 8605 2727 50  0000 C CNN
F 2 "" H 8600 2900 50  0001 C CNN
F 3 "" H 8600 2900 50  0001 C CNN
	1    8600 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 2900 8600 2850
Connection ~ 8600 2850
$Comp
L Device:C C5
U 1 1 5EDC13E7
P 9450 2450
F 0 "C5" H 9475 2550 50  0000 L CNN
F 1 "22uF" H 9475 2350 50  0000 L CNN
F 2 "Capacitor_SMD:C_1812_4532Metric" H 9488 2300 50  0001 C CNN
F 3 "" H 9450 2450 50  0001 C CNN
	1    9450 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	9200 2850 9450 2850
Wire Wire Line
	9450 2850 9450 2600
Connection ~ 9200 2850
Wire Wire Line
	9200 1900 9450 1900
Wire Wire Line
	9450 1900 9450 2300
Connection ~ 9200 1900
$Comp
L Device:C C4
U 1 1 5EDCA9CA
P 7600 2450
F 0 "C4" H 7625 2550 50  0000 L CNN
F 1 "22uF" H 7625 2350 50  0000 L CNN
F 2 "Capacitor_SMD:C_1812_4532Metric" H 7638 2300 50  0001 C CNN
F 3 "" H 7600 2450 50  0001 C CNN
	1    7600 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 2850 7850 2850
Wire Wire Line
	7600 2850 7600 2600
$Comp
L Device:R R6
U 1 1 5EDD8381
P 8050 2450
F 0 "R6" V 8130 2450 50  0000 C CNN
F 1 "10k" V 8050 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7980 2450 50  0001 C CNN
F 3 "" H 8050 2450 50  0001 C CNN
	1    8050 2450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8300 2450 8250 2450
Wire Wire Line
	8300 2250 8250 2250
Wire Wire Line
	7600 2250 7600 2300
Wire Wire Line
	7900 2450 7850 2450
Connection ~ 8250 2250
Wire Wire Line
	8250 1900 8250 2250
Wire Wire Line
	8250 1900 8450 1900
$Comp
L Connector_Generic:Conn_01x04 J2
U 1 1 5EDF1660
P 10300 2000
F 0 "J2" H 10380 1992 50  0000 L CNN
F 1 "Out" H 10380 1901 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 10300 2000 50  0001 C CNN
F 3 "~" H 10300 2000 50  0001 C CNN
	1    10300 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10100 1900 10050 1900
Connection ~ 9450 1900
Wire Wire Line
	10100 2000 9750 2000
Wire Wire Line
	9750 2000 9750 2850
Wire Wire Line
	9750 2850 9450 2850
Connection ~ 9450 2850
Wire Wire Line
	8250 2450 8250 2600
Connection ~ 8250 2450
Wire Wire Line
	8250 2450 8200 2450
Text GLabel 8250 2600 0    50   Input ~ 0
EN
Text GLabel 9950 2100 0    50   Input ~ 0
EN
Text GLabel 10050 2200 0    50   Input ~ 0
BAT+
Wire Wire Line
	10100 2100 9950 2100
Wire Wire Line
	10050 2200 10100 2200
Connection ~ 7600 2250
Text GLabel 7050 1950 2    39   Input ~ 0
BAT+
Wire Wire Line
	6900 2250 7600 2250
Wire Wire Line
	7050 1950 6900 1950
Connection ~ 6900 1950
Wire Wire Line
	4150 3150 4600 3150
Connection ~ 4150 3150
Connection ~ 4600 3150
Wire Wire Line
	4150 1950 4150 2100
Connection ~ 4150 2100
Wire Wire Line
	4150 1950 6900 1950
$Comp
L Device:C C1
U 1 1 5980CC73
P 1300 2650
F 0 "C1" H 1325 2750 50  0000 L CNN
F 1 "0.1uF" H 1325 2550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1338 2500 50  0001 C CNN
F 3 "" H 1300 2650 50  0001 C CNN
	1    1300 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 598D157A
P 6600 2250
F 0 "R5" V 6680 2250 50  0000 C CNN
F 1 "100" V 6600 2250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6530 2250 50  0001 C CNN
F 3 "" H 6600 2250 50  0001 C CNN
	1    6600 2250
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 598D0D68
P 4600 2700
F 0 "R4" V 4680 2700 50  0000 C CNN
F 1 "1k" V 4600 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4530 2700 50  0001 C CNN
F 3 "" H 4600 2700 50  0001 C CNN
	1    4600 2700
	-1   0    0    1   
$EndComp
$Comp
L Device:R R3
U 1 1 5980D069
P 3600 2500
F 0 "R3" V 3680 2500 50  0000 C CNN
F 1 "10k" V 3600 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3530 2500 50  0001 C CNN
F 3 "" H 3600 2500 50  0001 C CNN
	1    3600 2500
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5980CDB4
P 2050 2550
F 0 "R1" V 2130 2550 50  0000 C CNN
F 1 "1k" V 2050 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1980 2550 50  0001 C CNN
F 3 "" H 2050 2550 50  0001 C CNN
	1    2050 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5980CC1A
P 2050 2250
F 0 "R2" V 2130 2250 50  0000 C CNN
F 1 "1k" V 2050 2250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1980 2250 50  0001 C CNN
F 3 "" H 2050 2250 50  0001 C CNN
	1    2050 2250
	0    1    1    0   
$EndComp
Text GLabel 10050 1750 0    50   Input ~ 0
5Vout
Wire Wire Line
	10050 1750 10050 1900
Connection ~ 10050 1900
Wire Wire Line
	10050 1900 9450 1900
Wire Wire Line
	7600 2250 8250 2250
Wire Wire Line
	7850 2450 7850 2850
Connection ~ 7850 2850
Wire Wire Line
	7850 2850 7600 2850
$EndSCHEMATC
